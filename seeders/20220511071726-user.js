"use strict";
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash("password", salt);

    await queryInterface.bulkInsert("users", [
      {
        name: "superadmin",
        email: "su@email.com",
        password: hashPassword,
        role: "superadmin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "admin",
        email: "admin@email.com",
        password: hashPassword,
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "member",
        email: "member@email.com",
        password: hashPassword,
        role: "member",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("users", {
      [Op.or]: [{ name: "su" }, { name: "kirito" }, { name: "kayaba" }],
    });
  },
};
