const jwt = require("jsonwebtoken");

const verifyAdmin = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) return res.sendStatus(401);
  console.log(authHeader);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, decoded) => {
    if (error) return res.sendStatus(403);
    console.log("user role: " + decoded.role);
    if (decoded.role == "superadmin" || decoded.role == "admin") {
      req.email = decoded.email
      req.id = decoded.id
      req.name = decoded.name
      next();
    } else {
      console.log("NOT AUTHORIZED");
      res.sendStatus(401);
    }
  });
};

module.exports = verifyAdmin;
