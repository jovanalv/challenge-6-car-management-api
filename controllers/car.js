const { Cars, Users } = require("../models");

const getAllData = async (req, res) => {
    try {
        const car = await Cars.findAll({
            include: {
                model: Users,
                required: true,
                attributes: ['name']
            },
            attributes: ['id', 'model', 'rentperday', 'description', 'capacity', 'transmission', 'year']
        })
        res.status(200).json({
            message: "Success get all Cars data",
            statusCode: 200,
            data: car,
        });
    } catch (error) {
        console.log(error);
    }
};

const getById = async (req, res) => {
    const id = req.params.id;
    try {
        const car = await Cars.findOne({
            include: {
                model: Users,
                required: true,
                attributes: ['name']
            },
            where: { id: id },
        })
        res.status(200).json({
            message: "Success get Car data",
            statusCode: 200,
            data: car,
        });
    } catch (error) {
        console.log(error);
    }
};

const createData = async (req, res) => {
    try {
        await Cars.create({
            UserId: req.id,
            plate: req.body.plate,
            manufacture: req.body.manufacture,
            model: req.body.model,
            image: req.body.image,
            rentperday: req.body.rentperday,
            capacity: req.body.capacity,
            description: req.body.description,
            transmission: req.body.transmission,
            type: req.body.type,
            year: req.body.year,
            options: req.body.options,
            specs: req.body.specs,
            availableat: req.body.availableat
        })
        res.status(201).json({
            message: `Data mobil berhasil dibuat oleh ${req.name}`,
            statusCode: 201
        });
    } catch (error) {
        res.send("Failed create car data", error);
    }
};

const updateData = async (req, res) => {
    const id = req.params.id;
    try {
        await Cars.update({
            UserId: req.id,
            plate: req.body.plate,
            manufacture: req.body.manufacture,
            model: req.body.model,
            image: req.body.image,
            rentperday: req.body.rentperday,
            capacity: req.body.capacity,
            description: req.body.description,
            transmission: req.body.transmission,
            type: req.body.type,
            year: req.body.year,
            options: req.body.options,
            specs: req.body.specs,
            availableat: req.body.availableat
        }, {
            where: {
                id: id
            }
        });
        res.status(200).json({
            message: `Data mobil berhasil diubah oleh ${req.name}`,
            statusCode: 200
        });
    } catch (error) {
        res.send("Failed update car data", error);
    }
};

const deleteData = async (req, res) => {
    const id = req.params.id;
    try {
        await Cars.destroy({
            where: {
                id: id
            }
        });
        res.status(200).json({
            message: `Data mobil berhasil dihapus oleh ${req.name}`,
            statusCode: 204
        });
    } catch (error) {
        res.send("Failed delete car data", error);
    }
};

module.exports = {
    getAllData,
    getById,
    createData,
    updateData,
    deleteData,
};
