const { Users } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const getAllUser = async (req, res) => {
  try {
    const users = await Users.findAll({
      attributes: ["id", "name", "email", "role"],
    });
    res.status(200).json({
      status: true,
      data: users,
    });
  } catch (error) {
    console.log(error);
  }
};

const getUserByToken = async (req, res) => {
  const token = req.cookies.refreshToken
  console.log(token);
  try {
    const user = await Users.findOne({
      where: {
        refresh_token: token
      },
      attributes: ["id", "name", "email", "role"]
    })
    res.status(200).json({
      status: true,
      data: user,
    });
  } catch (error) {
    console.log(error);
  }
}

const registerUser = async (req, res) => {
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  try {
    await Users.create({
      name: req.body.name,
      email: req.body.email,
      password: hashPassword,
      role: "member",
    });
    res.json({
      message: "Register Berhasil",
    });
  } catch (error) {
    console.log(error);
  }
};

const registerAdmin = async (req, res) => {
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  try {
    await Users.create({
      name: req.body.name,
      email: req.body.email,
      password: hashPassword,
      role: "admin",
    });
    res.json({
      message: "Register Berhasil",
    });
  } catch (error) {
    console.log(error);
  }
};

const loginUser = async (req, res) => {
  try {
    const reqEmail = req.body.email;
    const reqPassword = req.body.password;

    const user = await Users.findOne({
      where: {
        email: reqEmail,
      },
    });

    const match = await bcrypt.compare(reqPassword, user.password);
    if (!match)
      return res.json({
        message: "Password Salah",
      });

    const { id, name, email, role } = user;

    const accessToken = jwt.sign(
      { id, name, email, role },
      process.env.ACCESS_TOKEN_SECRET,
      {
        expiresIn: "1d",
      }
    );
    const refreshToken = jwt.sign(
      { id, name, email, role },
      process.env.REFRESH_TOKEN_SECRET,
      {
        expiresIn: "1d",
      }
    );

    await Users.update(
      { refresh_token: refreshToken },
      {
        where: {
          id: id,
        },
      }
    );

    res
      .cookie("refreshToken", refreshToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
      })
      .json({
        accessToken: accessToken,
      });
  } catch (error) {
    res.json({ message: "Email tidak ditemukan" });
  }
};

const logout = async (req, res) => {
  const refreshToken = req.cookies.refreshToken;
  if (!refreshToken) return res.sendStatus(204);

  const user = await Users.findOne({
    where: {
      refresh_token: refreshToken,
    },
  });

  if (!user) return res.sendStatus(204);

  const id = user.id;

  await Users.update(
    { refresh_token: null },
    {
      where: {
        id: id,
      },
    }
  );

  res.clearCookie("refreshToken").sendStatus(200);
};

module.exports = {
  getAllUser,
  getUserByToken,
  registerUser,
  registerAdmin,
  loginUser,
  logout,
};
